package com.example.testproject.network

import com.example.testproject.models.responsemodel.ImagesResponse
import retrofit2.Response
import retrofit2.http.GET


interface ApiService {

    @GET("photos")     //
    suspend fun getImageList(): Response<ImagesResponse>
}