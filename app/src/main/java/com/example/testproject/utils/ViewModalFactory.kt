package com.example.testproject.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.testproject.repositories.ImageListRepository
import com.example.testproject.viewmodels.GetImageViewModel



class ViewModalFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GetImageViewModel::class.java)) {
            return GetImageViewModel(ImageListRepository()) as T
        }

        if (modelClass.isAssignableFrom(GetImageViewModel::class.java)) {
            return GetImageViewModel(ImageListRepository()) as T
        }


        throw IllegalArgumentException("Unknown class name")
    }
}