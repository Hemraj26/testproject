package com.example.testproject.utils

import android.R
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import java.io.IOException


class Utils {

    companion object {

        fun hideKeyboard(activity: Activity) {
            val view = (activity.findViewById<View>(R.id.content) as ViewGroup).getChildAt(0)
            hideKeyboard(activity, view)
        }

        private fun hideKeyboard(context: Context, view: View) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

        fun isOnline(context: Context): Boolean {
            val runtime = Runtime.getRuntime()
            try {
                val ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8")
                val exitValue = ipProcess.waitFor()
                return exitValue == 0
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }

            return false
        }

        fun showAlert(message: String?, context: Context?) {
            AlertDialog.Builder(context)
                .setTitle("Error")
                .setMessage(message) // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(
                    R.string.yes
                ) { dialog, which ->
                    // Continue with delete operation
                } // A null listener allows the button to dismiss the dialog and take no further action.
                .setIcon(R.drawable.ic_dialog_alert)
                .show()


        }

    }


}