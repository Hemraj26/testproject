package com.example.testproject.utils


import android.content.Context
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import com.example.testproject.R


object PDialog {
    private var alertDialog: AlertDialog? = null

    fun showProgressBar(context: Context) {
        val alertDialogBuilder = AlertDialog.Builder(context)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_progress, null)
        alertDialogBuilder.setView(view)
        alertDialog = alertDialogBuilder.create()
        alertDialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
        alertDialog?.setCancelable(true)
        alertDialog?.show()
    }

    fun hideProgressBar(context: Context) {
        alertDialog?.let {
            if(it.isShowing)
                it.dismiss()
            alertDialog = null
        }
    }
}