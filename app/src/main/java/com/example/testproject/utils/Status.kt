package com.example.testproject.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}