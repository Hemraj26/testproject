package com.example.testproject.utils

data class Resource<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T): Resource<T> = Resource(status = Status.SUCCESS, data, null)

        fun <T> error(data: T?, message: String): Resource<T> =
            Resource(status = Status.ERROR, data = null, message = message)

        fun <T> loading(data: T?): Resource<T> =
            Resource(Status.LOADING, data = null, message = null)
    }
}
