package com.example.testproject.repositories

import com.example.testproject.network.RetrofitBuilder


class ImageListRepository {
    suspend fun imageListApi() =
        RetrofitBuilder.apiService.getImageList()
}