package com.example.testproject.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testproject.models.responsemodel.ImagesResponse
import com.example.testproject.repositories.ImageListRepository
import com.example.testproject.utils.Resource
import kotlinx.coroutines.launch

import retrofit2.Response

class GetImageViewModel(private val repository: ImageListRepository): ViewModel()
{
    private val getimagesdata = MutableLiveData<Resource<Response<ImagesResponse>>>()
    fun getImageData() = getimagesdata

    fun getImagesData(){
        viewModelScope.launch {
            try {
                getimagesdata.postValue(Resource.loading(data = null))
                val apiResponse = repository.imageListApi()
                getimagesdata.postValue(Resource.success(data = apiResponse))
            } catch (e: Exception) {
                e.printStackTrace()
                getimagesdata.postValue(Resource.error(data = null, e.localizedMessage!!))
            }
        }
    }


}