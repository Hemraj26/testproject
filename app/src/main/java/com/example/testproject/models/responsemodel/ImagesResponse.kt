package com.example.testproject.models.responsemodel

class ImagesResponse : ArrayList<ImagesResponse.ImagesResponseItem>()
{
    data class ImagesResponseItem(
        val id: Int,
        val title: String,
        val thumbnailUrl: String,
        val url: String,
        val albumId: Int,
    )
}