package com.example.testproject.models.updatemodel

class TableListModel(val id: Int,
                     val title:String,
                     val thumbnailUrl: String)