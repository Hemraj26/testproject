package com.example.testproject.ui.main.adapters.imageadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

import com.example.testproject.databinding.ItemLayoutBinding
import com.example.testproject.models.responsemodel.ImagesResponse

import com.bumptech.glide.load.model.LazyHeaders

import com.bumptech.glide.load.model.GlideUrl

class ImageListAdapter(
    val context: Context,
    var listItem: List<ImagesResponse.ImagesResponseItem>,
    var adapterAddClickListener: ImageListAdapter.onCompareAddClickListener,
    var adapterRemoveClickListener: ImageListAdapter.onCompareRemoveClickListener) : RecyclerView.Adapter<ImageListAdapter.ImagesViewHolder>() {

    class ImagesViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        var  binding = ItemLayoutBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImagesViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(com.example.testproject.R.layout.item_layout, parent, false)
        return ImagesViewHolder(view)
    }

    override fun onBindViewHolder(holder: ImagesViewHolder, position: Int)
    {
        var item = listItem[position]
        val url = GlideUrl(
            item.thumbnailUrl, LazyHeaders.Builder()
                .addHeader("User-Agent", "your-user-agent")
                .build())
        Glide.with(context).load(url).into(holder.binding.ivImage)
        holder.binding.tvTitle.text=item.title
        holder.binding.tvId.text= item.id.toString()
        holder.binding.tvUrl.text=item.thumbnailUrl

        holder.binding.btnCompare.setOnClickListener {
            adapterAddClickListener.onClickCompareAdd(item.id,item.title,item.thumbnailUrl)

            holder.binding.btnCompare.visibility=View.GONE
            holder.binding.btnRemove.visibility=View.VISIBLE

        }

        holder.binding.btnRemove.setOnClickListener {
            adapterRemoveClickListener.onClickCompareRemove(item.id)

            holder.binding.btnCompare.visibility=View.VISIBLE
            holder.binding.btnRemove.visibility=View.GONE

        }
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    interface onCompareAddClickListener
    {
        fun onClickCompareAdd(id: Int, title: String, thumbnailUrl: String)
    }

    interface onCompareRemoveClickListener
    {
        fun onClickCompareRemove(position: Int)
    }



}