package com.example.testproject.ui.main.adapters.tableadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

import com.example.testproject.databinding.ItemTableBinding
import com.example.testproject.models.updatemodel.TableListModel

class TableListAdapter(
    val context: Context,
    var listItem: List<TableListModel>) : RecyclerView.Adapter<TableListAdapter.ImagesViewHolder>() {

    class ImagesViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        var  binding = ItemTableBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImagesViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(com.example.testproject.R.layout.item_table, parent, false)
        return ImagesViewHolder(view)
    }

    override fun onBindViewHolder(holder: ImagesViewHolder, position: Int)
    {
        var item = listItem[position]
        holder.binding.tvId.text=item.id.toString()
        holder.binding.tvTitle.text=item.title
        holder.binding.tvUrl.text=item.thumbnailUrl
    }

    override fun getItemCount(): Int {
        return listItem.size
    }





}