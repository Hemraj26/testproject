package com.example.testproject.ui.base

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.testproject.utils.Utils


abstract class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Utils.hideKeyboard(this)
    }


}