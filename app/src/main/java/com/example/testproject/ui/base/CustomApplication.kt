package com.example.testproject.ui.base

import android.app.Application

class CustomApplication : Application() {
    companion object {
        lateinit var application: Application

        fun getInstance(): Application {
            return application
        }
    }
    override fun onCreate() {
        super.onCreate()
        application = this
    }
}