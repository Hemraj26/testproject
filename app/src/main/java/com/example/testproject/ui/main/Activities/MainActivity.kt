package com.example.testproject.ui.main.Activities


import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider

import com.example.testproject.databinding.ActivityMainBinding
import com.example.testproject.ui.base.BaseActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testproject.R

import com.example.testproject.models.responsemodel.ImagesResponse
import com.example.testproject.models.updatemodel.TableListModel
import com.example.testproject.ui.main.adapters.imageadapter.ImageListAdapter
import com.example.testproject.ui.main.adapters.tableadapter.TableListAdapter
import com.example.testproject.utils.PDialog
import com.example.testproject.utils.Status
import com.example.testproject.utils.Utils
import com.example.testproject.utils.ViewModalFactory
import com.example.testproject.viewmodels.GetImageViewModel

class MainActivity : BaseActivity(), ImageListAdapter.onCompareAddClickListener,
    ImageListAdapter.onCompareRemoveClickListener {
    private lateinit var binding: ActivityMainBinding
    private lateinit var getimageViewModel: GetImageViewModel
    private lateinit var imagelistAdapter: ImageListAdapter
    private lateinit var tablelistadapter: TableListAdapter
    private var imageitemlist: ArrayList<ImagesResponse.ImagesResponseItem> = ArrayList()
    private var tableitemlist = mutableListOf<TableListModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)
        init()
        setupObserver()


    }

    private fun init() {
        getimageViewModel =
            ViewModelProvider(this, ViewModalFactory()).get(GetImageViewModel::class.java)
        hitImagesApi()
    }

    private fun hitImagesApi() {
        if (Utils.isOnline(this)) {
            getimageViewModel.getImagesData()  //api call
        } else {
            Utils.showAlert(getString(R.string.check_internet), this)
        }
    }

    private fun setupImageList() {
        imagelistAdapter = ImageListAdapter(this, imageitemlist, this, this)
        val layoutManager = GridLayoutManager(this, 3)
        binding.recyclerImages.layoutManager = layoutManager
        binding.recyclerImages.adapter = imagelistAdapter
    }


    private fun setupTableList() {
        binding.recyclerTable.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        tablelistadapter = TableListAdapter(this, tableitemlist)
        binding.recyclerTable.adapter = tablelistadapter
    }


    fun setupObserver() {
        getimageViewModel.getImageData().observe(this) {
            when (it.status) {
                Status.LOADING -> {
                    PDialog.showProgressBar(this)
                }
                Status.ERROR -> {
                    PDialog.hideProgressBar(this)
                    Utils.showAlert(getString(R.string.somthingwentwrong), this)
                }
                Status.SUCCESS -> {
                    PDialog.hideProgressBar(this)
                    val apiResponse = it.data!!
                    if (apiResponse != null) {
                        imageitemlist = apiResponse.body()!!
                        setupImageList()
                    }
                }
            }
        }

    }


    override fun onClickCompareAdd(id: Int, title: String, thumbnailUrl: String) {
        tableitemlist.add(
            TableListModel(
                id,
                title,
                thumbnailUrl
            )
        )
        setupTableList()
    }

    override fun onClickCompareRemove(id: Int) {
        if (tableitemlist.isNotEmpty()) {
            for (index in 0 until tableitemlist.size) {
                if (id == tableitemlist[index].id) {
                    tableitemlist.removeAt(index)
                    setupTableList()
                    break
                }
            }
        }
    }


}